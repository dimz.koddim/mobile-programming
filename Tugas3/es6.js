//No.1 (Arrow Function)
const golden = () => console.log("this is golden!!")
}
golden()
console.log("")

//No.2 (Object Literal di ES6)
const newFunction = (firstName, lastName) =>({
firstName: firstName,
lastName: lastName,
fullName: function(){
console.log(firstName + " " + lastName)
return
}
} 
)
//Driver Code
newFunction("William", "Imoh\n").fullName()

//No.3 (Destructuring)
const newObject = {
firstName: "Harry",
lastName: "Potter Holt",
destination: "Hogwarts React Conf",
occupation: "Deve-wizard Avocado",
spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)
console.log("")

//No.4 (Array Spreading)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)
console.log("")

//No.5 (Template Literals)
const planet = "earth"
const view = "glass"
var before = `Lorem '  ${view}  dolor sit amet, 
consectetur adipiscing elit,  ${planet}  do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
// Driver Code
console.log(before)